type O0<T> = Option<T>;
type O1<T> = O0<O0<T>>;
type O2<T> = O1<O1<T>>;
type O3<T> = O2<O2<T>>;
type O4<T> = O3<O3<T>>;
type O5<T> = O4<O4<T>>;
type O6<T> = O5<O5<T>>;
type O7<T> = O6<O6<T>>;
type O8<T> = O7<O7<T>>;
type O9<T> = O8<O8<T>>;

type Q128<T> = O7<T>;
type Q129<T> = O7<O0<T>>;
type Q253<T> = O7<O6<O5<O4<O3<O2<O0<T>>>>>>>;
type Q254<T> = O7<O6<O5<O4<O3<O2<O1<T>>>>>>>;
type Q255<T> = O7<O6<O5<O4<O3<O2<O1<O0<T>>>>>>>>;
type Q256<T> = O8<T>;
type Q257<T> = O8<O0<T>>;
type Q384<T> = O8<O7<T>>;
type Q510<T> = O8<O7<O6<O5<O4<O3<O2<O1<T>>>>>>>>;
type Q511<T> = O8<O7<O6<O5<O4<O3<O2<O1<O0<T>>>>>>>>>;
type Q512<T> = O9<T>;
type Q513<T> = O9<O0<T>>;

enum B {
    B0,
}
enum C {
    C0,
    C1,
}
enum D {
    D0,
    D1,
    D2,
}

fn f<T>(n: u32) {
    print!("{}:{} ", n, std::mem::size_of::<T>());
}

fn describe<T>() {
    f::<Q128<T>>(128);
    f::<Q129<T>>(129); // 1
    f::<Q253<T>>(253); // 1
    f::<Q254<T>>(254); // 1
    f::<Q255<T>>(255); // 1
    f::<Q256<T>>(256); // 2
    f::<Q257<T>>(257); // 2
    f::<Q384<T>>(384); // 2
    f::<Q510<T>>(510); // 2
    f::<Q511<T>>(511); // 3
                       // f::<Q512<T>>(512);
                       // f::<Q513<T>>(513);
    println!("");
}

fn main() {
    describe::<()>();
    describe::<B>();
    describe::<C>();
    describe::<D>();
}

// 128:1 129:1 253:1 254:1 255:1 256:2 257:2 384:2 510:2 511:3
// 128:1 129:1 253:1 254:1 255:1 256:2 257:2 384:2 510:2 511:3
// 128:1 129:1 253:1 254:1 255:2 256:2 257:2 384:2 510:3 511:3
// 128:1 129:1 253:1 254:2 255:2 256:2 257:2 384:2 510:3 511:3
